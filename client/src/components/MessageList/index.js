import React from 'react';
import PropTypes from 'prop-types';

import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import TextField from '@material-ui/core/TextField';

import { withStyles } from '@material-ui/core';

import handleInputChange from '../../utils/handleInputChange';

const styles = (theme) => ({
  listRoot: {
    flexGrow: 1,
    overflow: 'auto',
  },
});

class MessageList extends React.Component {
  state = {
    inputValue: '',
  };

  componentDidUpdate(prevProps) {
    const { channelId, fetchMessages } = this.props;

    if (prevProps.channelId !== channelId) {
      fetchMessages(channelId);
    }
  }

  handleSubmit() {
    this.props.createMessage(this.props.channelId, { content: this.state.inputValue })
    this.setState({ inputValue: '' });
  }

  handleKeyDown(event) {
    const { keyCode, shiftKey } = event;

    if (keyCode === 13 && !shiftKey) {
      this.handleSubmit();
      event.preventDefault();
    }
  }

  render() {
    const { classes, messages } = this.props;

    return (
      <React.Fragment>
        <List classes={{ root: classes.listRoot }}>
          {Object.entries(messages).map(([id, message]) =>
            <ListItem key={id}>
              {message.author}:<br />
              {message.content.split('\n').map((line, index) => (
                <React.Fragment key={index}>{line}<br /></React.Fragment>
              ))}
            </ListItem>
          )}
        </List>
        <TextField
          multiline
          fullWidth
          variant='outlined'
          name='inputValue'
          value={this.state.inputValue}
          // onChange={this.handleChange.bind(this)}
          onChange={handleInputChange.bind(this)}
          onKeyDown={this.handleKeyDown.bind(this)}
        />
      </React.Fragment>
    );
  }

  static propTypes = {
    classes: PropTypes.object.isRequired,
    channelId: PropTypes.string,
    messages: PropTypes.object.isRequired,
    fetchMessages: PropTypes.func.isRequired,
    createMessage: PropTypes.func.isRequired,
  };
}

export default withStyles(styles)(MessageList);
