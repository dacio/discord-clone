import React from 'react';
import PropTypes from 'prop-types';

import { withStyles, TextField, Button } from '@material-ui/core';

import handleInputChange from '../../utils/handleInputChange';

const styles = (theme) => ({
  submit: {
    marginTop: theme.spacing.unit * 2,
  },
});

class ChannelForm extends React.Component {
  state = {
    name: '',
  };

  handleSubmit(event) {
    event.preventDefault();

    this.props.submitCallback(this.state);
  }

  render() {
    const { classes } = this.props;

    return (
      <form onSubmit={this.handleSubmit.bind(this)}>
        <TextField
          name='name'
          label='Name'
          value={this.state.name}
          onChange={handleInputChange.bind(this)}
          fullWidth
        />
        <Button
          type='submit'
          color='secondary'
          variant='contained'
          fullWidth
          className={classes.submit}
        >
          Submit
        </Button>
      </form>
    );
  }

  static propTypes = {
    classes: PropTypes.object.isRequired,
    submitCallback: PropTypes.func.isRequired,
  };
}

export default withStyles(styles)(ChannelForm);
