import React from 'react';
import PropTypes from 'prop-types'

import { withStyles, Dialog, DialogTitle } from '@material-ui/core';

import ChannelForm from './ChannelForm';

const styles = (theme) => ({
  dialogPaper: {
    padding: theme.spacing.unit * 2,
  },
});

class ChannelDialog extends React.Component {
  submitCallback(data) {
    this.props.create(data);
  }

  render() {
    const { classes, open, onClose } = this.props;

    return (
      <Dialog
        classes={{ paper: classes.dialogPaper }}
        {...{open, onClose}}
      >
        <DialogTitle>Hello!</DialogTitle>
        <ChannelForm submitCallback={this.submitCallback.bind(this)} />
      </Dialog>
    );
  }

  static propTypes = {
    classes: PropTypes.object.isRequired,

    open: PropTypes.bool.isRequired,

    onClose: PropTypes.func.isRequired,
    create: PropTypes.func.isRequired,
  };
}

export default withStyles(styles)(ChannelDialog);
