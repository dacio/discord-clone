import React from 'react';
import PropTypes from 'prop-types';

import Drawer from '@material-ui/core/Drawer';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import Button from '@material-ui/core/Button';

import AddIcon from '@material-ui/icons/Add';

import { withStyles } from '@material-ui/core';

import ChannelDialog from './ChannelDialog';

const styles = (theme) => {
  const drawerWidth = theme.spacing.unit * 16;

  return {
    drawer: {
      width: drawerWidth,
      flexShrink: 0,
    },
    drawerPaper: {
      width: drawerWidth,
      overflowX: 'hidden',
      textOverflow: 'ellipsis',
    },
    drawerPaperAnchorLeft: {
      left: theme.spacing.unit * 9,
    },
    toolbar: theme.mixins.toolbar,
  }
};

class ChannelDrawer extends React.Component {
  state = {
    dialogOpen: false,
  };

  closeDialog() {
    this.setState({ dialogOpen: false });
  }

  openDialog() {
    this.setState({ dialogOpen: true });
  }

  componentDidUpdate(prevProps) {
    const { guildId, load } = this.props;

    if (guildId !== prevProps.guildId && guildId) {
      load(guildId);
    }
  }

  render() {
    const {
      classes,
      channels, guildId,
      create, set,
    } = this.props;
    const { dialogOpen } = this.state;

    return (
      <React.Fragment>
        <ChannelDialog
          open={dialogOpen}
          onClose={this.closeDialog.bind(this)}
          create={(data) => create(guildId, data)}
        />
        <Drawer
          className={classes.drawer}
          classes={{
            paper: classes.drawerPaper,
            paperAnchorDockedLeft: classes.drawerPaperAnchorLeft,
          }}
          variant="permanent"
        >
          <div className={classes.toolbar} />
          <List>
            <ListItem>
              Channels
              <Button onClick={this.openDialog.bind(this)}>
                <AddIcon />
              </Button>
            </ListItem>
            {Object.entries(channels).map(([id, channel]) => (
              <ListItem button onClick={() => set(id)} key={id}>#{channel.name}</ListItem>
            ))}
          </List>
        </Drawer>
      </React.Fragment>
    );
  }

  static propTypes = {
    classes: PropTypes.object.isRequired,

    guildId: PropTypes.string,
    channels: PropTypes.object.isRequired,

    load: PropTypes.func.isRequired,
    create: PropTypes.func.isRequired,
    set: PropTypes.func.isRequired,
  };
}

export default withStyles(styles)(ChannelDrawer);
