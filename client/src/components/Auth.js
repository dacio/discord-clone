import React from 'react';
import PropTypes from 'prop-types';

import Dialog from '@material-ui/core/Dialog';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';

import PersonIcon from '@material-ui/icons/Person';
import AssignmentIndIcon from '@material-ui/icons/AssignmentInd';

import { withStyles } from '@material-ui/core';

import handleTabChange from '../utils/handleTabChange';
import handleFormSubmit from '../utils/handleFormSubmit';

const styles = (theme) => ({
  dialogPaper: {
    padding: theme.spacing.unit * 2,
  },
  tab: {
    width: '50%',
  },
  submit: {
    marginTop: theme.spacing.unit * 2,
  },
});

class Auth extends React.Component {
  state = {
    tab: 'login',
    open: false,
  };

  static getDerivedStateFromProps(nextProps, prevState) {
    if (nextProps.loggedIn && prevState.open) {
      return { ...prevState, open: false };
    } else {
      return { ...prevState };
    }
  }

  submitCallback(data) {
    const { login, register } = this.props;
    const { tab } = this.state;

    switch(tab) {
      case 'login': {
        login(data);
        break;
      }
      case 'register': {
        register(data);
        break;
      }
      default: {
        break;
      }
    }
  }

  handleClick() {
    const { loggedIn, signout } = this.props;
    if (loggedIn) {
      signout();
    } else {
      this.setState({ open: true });
    }
  }

  handleClose() {
    this.setState({ open: false });
  }

  render() {
    const { classes, loggedIn } = this.props;
    const { tab, open } = this.state;

    return (
      <React.Fragment>
        <Button
          color='secondary'
          variant='contained'
          onClick={this.handleClick.bind(this)}
        >
          { loggedIn ? 'Signout' : 'Login/Register' }
        </Button>
        <Dialog
          open={open}
          maxWidth='xs'
          fullWidth
          classes={{ paper: classes.dialogPaper }}
          onClose={this.handleClose.bind(this)}
        >
          <Tabs
            value={tab}
            onChange={handleTabChange.bind(this)}
            indicatorColor='primary'
            textColor='primary'
            variant='fullWidth'
          >
            <Tab
              label='Login'
              value='login'
              icon={<PersonIcon />}
              className={classes.tab}
            />
            <Tab
              label='Register'
              value='register'
              icon={<AssignmentIndIcon />}
              className={classes.tab}
            />
          </Tabs>
          <form onSubmit={handleFormSubmit(this.submitCallback.bind(this))}>
            <TextField
              label='Username'
              name='username'
              fullWidth
            />
            <TextField
              label='Password'
              name='password'
              type='password'
              fullWidth
            />
            <Button
              type='submit'
              variant='contained'
              color='secondary'
              fullWidth
              className={classes.submit}
            >
              {tab}
            </Button >
          </form>
        </Dialog>
      </React.Fragment>
    );
  }

  static propTypes = {
    classes: PropTypes.object.isRequired,

    loggedIn: PropTypes.bool.isRequired,

    login: PropTypes.func.isRequired,
    register: PropTypes.func.isRequired,
    signout: PropTypes.func.isRequired,
  }
}

export default withStyles(styles)(Auth);
