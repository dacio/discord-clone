import React from 'react';
import PropTypes from 'prop-types';

import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';

import { withStyles } from '@material-ui/core';

import GuildForm from './GuildForm';

const styles = (theme) => ({
  dialogPaper: {
    padding: theme.spacing.unit * 2,
  },
});

class GuildDialog extends React.Component {
  submitCallback(data) {
    const { guild, post, update, onClose } = this.props;

    if (guild) {
      update(guild._id, data);
    } else {
      post(data);
    }

    onClose();
  }

  render() {
    const { classes, open, guild, onClose } = this.props;

    return (
      <Dialog
        open={open}
        onClose={onClose}
        classes={{ paper: classes.dialogPaper }}
      >
        <DialogTitle>
          { guild ? 'Edit Guild' : 'New Guild' }
        </DialogTitle>
        <GuildForm guild={guild} submitCallback={this.submitCallback.bind(this)} />
      </Dialog>
    );
  }

  static propTypes = {
    classes: PropTypes.object.isRequired,

    guild: PropTypes.object,

    onClose: PropTypes.func.isRequired,
    post: PropTypes.func.isRequired,
    update: PropTypes.func.isRequired,
  }
}

export default withStyles(styles)(GuildDialog);
