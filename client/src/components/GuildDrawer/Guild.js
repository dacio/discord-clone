import React from 'react';
import PropTypes from 'prop-types';

import ListItem from '@material-ui/core/ListItem';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Avatar from '@material-ui/core/Avatar';

import { withStyles } from '@material-ui/core';

const styles = (theme) => ({});

class Guild extends React.Component {
  handleContextMenu(event) {
    event.preventDefault();
    this.props.openContexMenu(this);
  }

  render() {
    const { guild, onClick } = this.props;

    return (
      <ListItem
        button
        onClick={onClick}
        onContextMenu={this.handleContextMenu.bind(this)}
      >
        <ListItemAvatar>
          <Avatar>
            {guild.name.split(' ').map((word) => word[0]).join('')}
          </Avatar>
        </ListItemAvatar>
      </ListItem>
    );
  }

  static propTypes = {
    classes: PropTypes.object.isRequired,
    guild: PropTypes.object.isRequired,
  };
}

export default withStyles(styles)(Guild);
