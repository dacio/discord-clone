import React from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';

import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import ListItem from '@material-ui/core/ListItem';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Avatar from '@material-ui/core/Avatar';

import AddIcon from '@material-ui/icons/Add';

import { withStyles } from '@material-ui/core';

import GuildDialog from './GuildDialog';

const styles = (theme) => ({});

class GuildsWrapper extends React.Component {
  state = {
    guild: null,
    guildRef: null,
    dialogOpen: false,
  };

  openContextMenu(guildComponent) {
    this.setState({
      guild: guildComponent.props.guild,
      guildRef: ReactDOM.findDOMNode(guildComponent),
    });
  }

  closeContextMenu() {
    this.setState({
      guildRef: null,
    });
  }

  openDialogMenu() {
    this.setState({ dialogOpen: true });
  }

  closeDialogMenu() {
    this.setState({
      dialogOpen: false,
      guild: null,
    });
  }

  handleClickDelete() {
    this.props.del(this.state.guild._id);
    this.setState({ guild: null });
    this.closeContextMenu();
  }

  handleClickEdit() {
    this.openDialogMenu();
    this.closeContextMenu();
  }

  render() {
    const { children, post, update } = this.props;
    const { guild, guildRef, dialogOpen } = this.state;

    return (
      <React.Fragment>
        <Menu
          anchorEl={guildRef}
          open={Boolean(guildRef)}
          onClose={this.closeContextMenu.bind(this)}
        >
          <MenuItem onClick={this.handleClickDelete.bind(this)}>Delete</MenuItem>
          <MenuItem onClick={this.handleClickEdit.bind(this)}>Edit</MenuItem>
        </Menu>
        <GuildDialog
          open={dialogOpen}
          onClose={this.closeDialogMenu.bind(this)}
          {...{guild, post, update}}
        />
        {
          React.Children.map(children, (child) =>
            React.cloneElement(child, { openContexMenu: this.openContextMenu.bind(this) })
          )
        }

        <ListItem button onClick={this.openDialogMenu.bind(this)}>
          <ListItemAvatar>
            <Avatar>
              <AddIcon />
            </Avatar>
          </ListItemAvatar>
        </ListItem>
      </React.Fragment>
    );
  }

  static propTypes = {
    children: PropTypes.array,
    del: PropTypes.func.isRequired,
    post: PropTypes.func.isRequired,
    update: PropTypes.func.isRequired,
  };
}

export default withStyles(styles)(GuildsWrapper);
