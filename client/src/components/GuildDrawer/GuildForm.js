import React from 'react';
import PropTypes from 'prop-types';

import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';

import { withStyles } from '@material-ui/core';

import handleInputChange from '../../utils/handleInputChange';

const styles = (theme) => ({
  submit: {
    marginTop: theme.spacing.unit * 2,
  },
});

class GuildForm extends React.Component {
  state = {
    name: '',
  };

  componentDidMount() {
    const { guild } = this.props;

    if(guild) {
      this.setState({
        name: guild.name,
      });
    }
  }

  handleSubmit(event) {
    event.preventDefault();

    this.props.submitCallback(this.state);
  }

  render() {
    const { classes } = this.props;

    return (
      <form onSubmit={this.handleSubmit.bind(this)}>
        <TextField
          name='name'
          label='Name'
          fullWidth
          value={this.state.name}
          onChange={handleInputChange.bind(this)}
        />
        <Button
          type='submit'
          color='secondary'
          variant='contained'
          fullWidth
          className={classes.submit}
        >
          Submit
        </Button>
      </form>
    );
  }

  static propTypes = {
    classes: PropTypes.object.isRequired,
    submitCallback: PropTypes.func.isRequired,
    guild: PropTypes.object,
  }
}

export default withStyles(styles)(GuildForm);
