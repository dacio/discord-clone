import isElectron from 'is-electron';
import axios from 'axios';

axios.defaults.baseURL = isElectron() ? 'https://desolate-mesa-38612.herokuapp.com' : '';
