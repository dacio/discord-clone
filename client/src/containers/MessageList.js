import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import { fetchMessages, createMessage } from '../ducks/messages';
import MessageListComponent from '../components/MessageList';

const mapStateToProps = (state) => ({
  channelId: state.app.channel,
  messages: state.messages,
})

const mapDispatchToProps = (dispatch) => bindActionCreators({
  fetchMessages, createMessage
}, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(MessageListComponent);
