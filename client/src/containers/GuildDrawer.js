import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import ReactDrawerComponent from '../components/GuildDrawer';
import { loadRequest as load, del, post, update } from '../ducks/guilds';
import { setGuild } from '../ducks/app';

const mapStateToProps = (state) => ({
  guilds: state.guilds
});

const mapDispatchToProps = (dispatch) => bindActionCreators({
  load, del, post, update, set: setGuild,
}, dispatch)

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ReactDrawerComponent);
