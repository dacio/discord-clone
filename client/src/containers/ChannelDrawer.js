import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import ChannelDrawerComponent from '../components/ChannelDrawer';

import { load, create } from '../ducks/channels';
import { setChannel } from '../ducks/app';

const mapStateToProps = (state) => ({
  guildId: state.app.guild,
  channels: state.channels,
});

const mapDispatchToProps = (dispatch) => bindActionCreators({
  load, create, set: setChannel,
}, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ChannelDrawerComponent);
