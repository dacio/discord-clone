import io from 'socket.io-client';
import axios from 'axios';

import store from '../ducks';
import { add } from '../ducks/channels';

let guild = null;

store.subscribe(() => {
  guild = store.getState().app.guild;
});

// const socket = io('/channels')
const socket = io(`${axios.defaults.baseURL}/channels`)

socket.on('new', (channel) => {
  if (channel.guild === guild) {
    store.dispatch(add(channel));
  }
});
