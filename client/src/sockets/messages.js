import io from 'socket.io-client';
import axios from 'axios';

import { addMessages } from '../ducks/messages';
import store from '../ducks';

// const socket = io('/messages');
const socket = io(`${axios.defaults.baseURL}/messages`)

let channel = null;

store.subscribe(() => {
  channel = store.getState().app.channel;
});

socket.on('new', (message) => {
  if (message.channel === channel) {
    store.dispatch(addMessages(message));
  }
});
