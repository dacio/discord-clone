import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import AppBar from '@material-ui/core/AppBar';
import CssBaseline from '@material-ui/core/CssBaseline';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';

import { withStyles } from '@material-ui/core/styles';

import AuthContainer from './containers/Auth';
import GuildDrawerContainer from './containers/GuildDrawer';
import ChannelDrawerContainer from './containers/ChannelDrawer';
import MessageListContainer from './containers/MessageList';

const styles = theme => ({
  root: {
    display: 'flex',
    height: '100vh',
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing.unit * 3,
    display: 'flex',
    flexDirection: 'column',
  },
  name: {
    flexGrow: 1
  },
  toolbar: theme.mixins.toolbar,
});

class App extends React.Component {
  render() {
    const { classes, loggedIn } = this.props;

    return (
      <div className={classes.root}>
        <CssBaseline />
        <AppBar position="fixed" className={classes.appBar}>
          <Toolbar>
            <Typography
              variant="h6"
              color="inherit"
              noWrap
              className={classes.name}
            >
              Harmony
            </Typography>
            <AuthContainer loggedIn={loggedIn} />
          </Toolbar>
        </AppBar>
        {loggedIn && <GuildDrawerContainer />}
        {loggedIn && <ChannelDrawerContainer />}
        <main className={classes.content}>
          <div className={classes.toolbar} />
          {loggedIn && <MessageListContainer />}
        </main>
      </div>
    );
  }

  static propTypes = {
    classes: PropTypes.object.isRequired,
    loggedIn: PropTypes.bool.isRequired,
  }
}

const mapStateToProps = (state) => ({
  loggedIn: state.app.token !== null,
});

export default connect(mapStateToProps)(withStyles(styles)(App));
