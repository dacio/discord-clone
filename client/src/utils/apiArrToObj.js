export default function apiArrToObj(arr) {
  return arr.reduce((combined, currObj) => (
    { ...combined, [currObj._id]: currObj }
  ), {});
}
