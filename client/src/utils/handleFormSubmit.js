export default function(callback) {
  return (event) => {
    event.preventDefault();

    const formData = new FormData(event.target);
    const data = Array.from(formData.entries()).reduce((obj, [key, value]) => ({
      ...obj, [key]: value
    }), {});

    callback(data);
  }
}
