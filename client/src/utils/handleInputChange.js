export default function(event) {
  const { target } = event;
  this.setState({ [target.name]: target.value });
}
