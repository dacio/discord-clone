import axios from 'axios';

import { setError } from './app';
import arrToObj from '../utils/apiArrToObj';

// Action Types
const LOAD = 'discord-clone/channels/LOAD';
const ADD = 'discord-clone/channels/ADD';

const initialState = {}

export default function reducer(state=initialState, action) {
  switch(action.type) {
    case LOAD:
      return arrToObj(action.channels)
    case ADD:
      const newChannels = arrToObj(action.channels);
      return { ...state, ...newChannels };
    default:
      return state;
  }
}

// Action Creators
export function set(channels) {
  return {
    type: LOAD,
    channels,
  }
}

export function add(...channels) {
  return {
    type: ADD,
    channels,
  }
}

// Thunks
export function load(guildId) {
  return (dispatch) => {
    axios.get(`/api/guilds/${guildId}/channels`)
      .then((res) => {
        dispatch(set(res.data));
      })
      .catch((err) => {
        dispatch(setError(err.response.data));
      });
  }
}

export function create(guildId, channel) {
  return (dispatch) => {
    axios.post(`/api/guilds/${guildId}/channels`, channel)
      .catch((err) => {
        dispatch(setError(err.response.data));
      });
  }
}
