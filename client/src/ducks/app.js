import axios from 'axios';

// Action Types
const AUTH_SET = 'discord-clone/app/AUTH_SET';
const AUTH_SIGNOUT = 'discord-clone/app/AUTH_SIGNOUT';

const SET_ERROR = 'discord-clone/app/SET_ERROR';
const SET_GUILD = 'discord-clone/app/SET_GUILD';
const SET_CHANNEL = 'discord-clone/app/SET_CHANNEL';

const token = localStorage.getItem('token');

if(token) {
  axios.defaults.headers.common['Authorization'] = `Bearer ${token}`;
}

const initialState = {
  token: localStorage.getItem('token'),
  guild: null,
  channel: null,
  error: null,
};

export default function reducer(state=initialState, action) {
  switch(action.type) {
    case AUTH_SET: {
      return { ...state, token: action.token };
    }
    case SET_ERROR: {
      return { ...state, error: action.error };
    }
    case AUTH_SIGNOUT: {
      return { ...initialState, token: null };
    }
    case SET_GUILD: {
      return { ...state, guild: action.guild };
    }
    case SET_CHANNEL: {
      return { ...state, channel: action.channel };
    }
    default: {
      return { ...state };
    }
  }
}

// Action Creators
export function setError(error) {
  return {
    type: SET_ERROR,
    error,
  };
}

export function setAuth(token) {
  localStorage.setItem('token', token);
  axios.defaults.headers.common['Authorization'] = `Bearer ${token}`;

  return {
    type: AUTH_SET,
    token,
  };
}

export function signout() {
  localStorage.removeItem('token');
  delete axios.defaults.headers.common['Authorization'];

  return {
    type: AUTH_SIGNOUT,
  };
}

export function setGuild(guild) {
  return {
    type: SET_GUILD,
    guild,
  };
}

export function setChannel(channel) {
  return {
    type: SET_CHANNEL,
    channel,
  };
}

// Thunks
export function register({ username, password }) {
  return (dispatch) => {
    axios.post('/api/auth/register', { username, password })
      .then((res) => {
        dispatch(setAuth(res.data.token));
      })
      .catch((err) => {
        dispatch(setError(err.response.data));
      });
  }
}

export function login({ username, password }) {
  return (dispatch) => {
    axios.post('/api/auth/login', { username, password })
      .then((res) => {
        dispatch(setAuth(res.data.token));
      })
      .catch((err) => {
        dispatch(setError(err.response.data));
      });
  }
}
