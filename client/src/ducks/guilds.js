import axios from 'axios';

import { setError } from './app';
import arrToObj from '../utils/apiArrToObj';

// Action Types
const LOAD = 'discord-clone/guilds/LOAD';
const ADD = 'discord-clone/guilds/ADD';
const MODIFY = 'discord-clone/guilds/MODIFY';
const REMOVE = 'discord-clone/guilds/REMOVE';

const intialState = {};

export default function reducer(state=intialState, action) {
  switch(action.type) {
    case LOAD: {
      return arrToObj(action.guilds);
    }
    case ADD: {
      const newGuilds = arrToObj(action.guilds);
      return { ...state, ...newGuilds };
    }
    case MODIFY: {
      const { id, data } = action;

      if (id in state) {
        return { ...state, [id]: { ...state[id], ...data } };
      } else {
        return { ...state };
      }
    }
    case REMOVE: {
      const { [action.id]: removedGuild, ...guilds } = state;
      return guilds;
    }
    default: {
      return { ...state };
    }
  }
}

// Action Creators
export function load(...guilds) {
  return {
    type: LOAD,
    guilds,
  };
}

export function add(...guilds) {
  return {
    type: ADD,
    guilds,
  };
}

export function remove(id) {
  return {
    type: REMOVE,
    id,
  };
}

export function modify(id, data) {
  return {
    type: MODIFY,
    id,
    data,
  }
}

// Thunks
export function loadRequest(token) {
  return (dispatch) => {
    axios.get('/api/guilds')
      .then((res) => {
        dispatch(load(...res.data));
      })
      .catch((err) => {
        dispatch(setError(err.response.data));
      });
  }
}

export function del(guildId) {
  return (dispatch) => {
    axios.delete(`/api/guilds/${guildId}`)
      .catch((err) => {
        dispatch(setError(err.response.data));
      });
  }
}

export function update(guildId, data) {
  return (dispatch) => {
    axios.patch(`/api/guilds/${guildId}`, data)
      .catch((err) => {
        dispatch(setError(err.response.data));
      })
  }
}

export function post(guild) {
  return (dispatch) => {
    axios.post('/api/guilds', guild)
      .catch((err) => {
        dispatch(setError(err.response.data));
      });
  }
}
