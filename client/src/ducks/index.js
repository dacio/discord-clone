import { combineReducers, createStore, applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension'
import thunk from 'redux-thunk';

import app from './app';
import guilds from './guilds';
import channels from './channels';
import messages from './messages';

const rootReducer = combineReducers({
  app,
  guilds,
  channels,
  messages,
});

export default createStore(
  rootReducer,
  composeWithDevTools(applyMiddleware(thunk))
);
