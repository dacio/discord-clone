module.exports = function toUpdateObject() {
  const obj = this.toObject();
  const modified = this.modifiedPaths();

  const modifedObj = modified.reduce((newObj, key) => (
    { ...newObj, [key]: obj[key] }
  ), {});

  return modifedObj;
};
