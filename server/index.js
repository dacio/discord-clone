const path = require('path');
const mongoose = require('mongoose');
const express = require('express');
const cors = require('cors');
const socketio = require('socket.io');

require('dotenv').config({ path: path.join(__dirname, '.env') });

const routes = require('./routes');

mongoose.connect(process.env.MONGODB_URI || 'mongodb://localhost/discord-clone', { useNewUrlParser: true });

const app = express();

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use('/api', routes);

if (process.env.NODE_ENV !== 'development') {
  app.use(express.static(path.join(__dirname, '../client/build')));

  app.get('*', (req, res) => {
    res.sendFile(path.join(__dirname, '../client/build/index.html'));
  });
}

const port = process.env.PORT || 5000;
const server = app.listen(port);

app.io = socketio(server);

// Initialize WebSocket namespaces for subscribing
['/guilds', '/channels', '/messages'].forEach((nsp) => {
  app.io.of(nsp);
});
