const express = require('express');
const jwt = require('express-jwt');

const auth = require('./auth');
const users = require('./users');
const guilds = require('./guilds');
const channels = require('./channels');
const messages = require('./messages');

const router = express.Router();
const authenticate = jwt({ secret: process.env.SECRET });

router.use('/auth', auth);

router.use(authenticate);
router.use('/users', users);
router.use('/guilds', guilds);
router.use('/guilds/:guildId/channels', channels);
router.use('/channels/:channelId/messages', messages);

module.exports = router;
