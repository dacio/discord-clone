const express = require('express');
const asyncHandler = require('express-async-handler');

const Guild = require('../models/guild');

const router = express.Router();

router.get('/', asyncHandler(async (req, res) => {
  const guilds = await Guild.find();

  res.json(guilds);
}));

router.post('/', asyncHandler(async (req, res) => {
  const { name } = req.body;
  const { _id: owner } = req.user;

  const guild = await Guild.create({ name, owner });

  res.json(guild);
  req.app.io.of('/guilds').emit('new', guild);
}));

router.patch('/:id', asyncHandler(async (req, res) => {
  const { name } = req.body;
  const { id } = req.params;

  const guild = await Guild.findById(id);

  if (guild.owner.toString() === req.user._id) {
    if (name) {
      guild.name = name;
    }

    req.app.io.of('/guilds').emit('modify', {
      id,
      data: guild.toUpdateObj(),
    });

    await guild.save();

    res.json(guild);
  } else {
    res.status(401).json({ error: 'Current user doesn\'t own this guild' });
  }
}));

router.delete('/:id', asyncHandler(async (req, res) => {
  const { id } = req.params;

  const guild = await Guild.findByIdAndDelete(id);

  res.json(guild);
  req.app.io.of('/guilds').emit('remove', id);
}));

module.exports = router;
