const express = require('express');
const asyncHandler = require('express-async-handler');

const Channel = require('../models/channel');
const Message = require('../models/message');

const router = express.Router({ mergeParams: true });

router.get('/', asyncHandler(async (req, res) => {
  const { channelId } = req.params;

  const messages = await Message.find({ channel: channelId });

  res.json(messages);
}));

router.post('/', asyncHandler(async (req, res) => {
  const { content } = req.body;
  const { channelId } = req.params;
  const { _id: author } = req.user;

  const [message, channel] = await Promise.all([
    Message.create({ content, channel: channelId, author }),
    Channel.findById(channelId),
  ]);

  channel.messages.push(message._id);

  await channel.save();

  res.json(message);
  req.app.io.of('/messages').emit('new', message);
}));

router.patch('/:id', asyncHandler(async (req, res) => {
  const { content } = req.body;
  const { id } = req.params;

  const message = await Message.findById(id);

  if (message.author.toString() === req.user._id) {
    if (content) {
      message.content = content;
    }

    await message.save();

    const messageJSON = message.toJSON();

    res.send(messageJSON);
    req.app.io.to(message.channel).emit('update message', messageJSON);
  } else {
    res.status(401).json({ error: 'Current user did not author this comment' });
  }
}));

module.exports = router;
