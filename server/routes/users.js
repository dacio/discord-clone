const express = require('express');
const asyncHandler = require('express-async-handler');

const User = require('../models/user');

const router = express.Router();

router.get('/:id', asyncHandler(async (req, res) => {
  const user = await User.findById(req.params.id);

  res.send(user.toJSON());
}));

router.patch('/:id', asyncHandler(async (req, res) => {
  const { username, password } = req.body;

  const user = await User.findById(req.params.id);

  if (req.user._id === user._id) {
    if (username) {
      user.username = username;
    }
    if (password) {
      user.password = password;
    }
    await user.save();

    res.send(user.toJSON());
  } else {
    res.status(401).json({ error: 'Cannot change another user\'s info' });
  }
}));

module.exports = router;
