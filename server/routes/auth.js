const express = require('express');
const asyncHandler = require('express-async-handler');
const jwt = require('jsonwebtoken');

const router = express.Router();

const User = require('../models/user');

router.post('/login', asyncHandler(async (req, res) => {
  const { email, password } = req.body;
  const user = await User.findOne({ email });

  if (!user) {
    res.status(400).json({ email: 'Email does not exist' });
  } else if (await user.comparePassword(password)) {
    res.json({ token: jwt.sign({ _id: user._id }, process.env.SECRET) });
  } else {
    res.status(400).json({ password: 'Password is invalid' });
  }
}));

router.post('/register', asyncHandler(async (req, res) => {
  const { username, password } = req.body;

  const user = await User.create({ username, password });

  res.json({ token: jwt.sign({ _id: user._id }, process.env.SECRET) });
}));

module.exports = router;
