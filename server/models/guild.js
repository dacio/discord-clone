const mongoose = require('mongoose');
const Channel = require('./channel');
const toUpdateObj = require('../utils/toUpdateObj');

const { Schema } = mongoose;

const GuildSchema = Schema({
  name: {
    type: String,
    required: true,
  },
  owner: {
    type: Schema.Types.ObjectId,
    required: true,
  },
  channels: [{
    type: Schema.Types.ObjectId,
    ref: 'Channel',
  }],
});

GuildSchema.pre('remove', function deleteChannels(next) {
  Channel.deleteMany({ _id: { $in: this.channels } }, next);
});

GuildSchema.methods.toUpdateObj = toUpdateObj;

module.exports = mongoose.model('Guild', GuildSchema);
