const mongoose = require('mongoose');
const Message = require('./message');
const toUpdateObj = require('../utils/toUpdateObj');

const { Schema } = mongoose;

const ChannelSchema = Schema({
  name: {
    type: String,
    required: true,
    unique: false,
  },
  guild: {
    type: Schema.Types.ObjectId,
    ref: 'Guild',
    required: true,
    unique: false,
  },
  messages: [{
    type: Schema.Types.ObjectId,
    ref: 'Message',
  }],
});

ChannelSchema.index({ name: 1, guild: 1 }, { unique: true });

// TODO: Remove Channel from list in parent Guild
ChannelSchema.pre('remove', function deleteMessages(next) {
  Message.deleteMany({ _id: { $in: this.messages } }, next);
});

ChannelSchema.methods.toUpdateObj = toUpdateObj;

module.exports = mongoose.model('Channel', ChannelSchema);
