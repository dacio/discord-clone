const mongoose = require('mongoose');
const bcrypt = require('bcrypt');

const { Schema } = mongoose;

// Remove hashed password from requests
const removePass = {
  transform(doc, ret) {
    delete ret.password; // eslint-disable-line no-param-reassign
  },
};

const UserSchema = Schema({
  username: {
    type: String,
    trim: true,
    required: true,
  },
  discriminator: {
    type: String,
    required: true,
  },
  password: {
    type: String,
    required: true,
  },
}, {
  toJSON: removePass,
  toObject: removePass,
});

UserSchema.index({ username: 1, discriminator: 1 }, { unique: true });

const discriminators = new Set([...Array(9999).keys()].map(key => String(key + 1).padStart(4, '0')));

UserSchema.pre('validate', async function genDiscrim(next) {
  try {
    const user = this;
    const User = user.constructor;

    if (user.isModified('username')) {
      const used = new Set(await User.find({ username: user.username, _id: { $ne: user._id } }).distinct('discriminator'));

      if (!user.discriminator || user.discriminator in used) {
        const unused = [...discriminators].filter(d => !used.has(d));

        user.discriminator = unused[Math.floor(Math.random() * unused.length)];
      }
    }

    next();
  } catch (err) {
    next(err);
  }
});

UserSchema.pre('save', async function hashPass(next) {
  try {
    const user = this;

    if (user.isModified('password')) {
      user.password = await bcrypt.hash(user.password, 10);
    }

    next();
  } catch (err) {
    next(err);
  }
});

UserSchema.methods.comparePassword = function compare(candidatePassword) {
  return bcrypt.compare(candidatePassword, this.password);
};

module.exports = mongoose.model('User', UserSchema);
