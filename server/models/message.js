const mongoose = require('mongoose');

const { Schema } = mongoose;

const MessageSchema = Schema({
  content: {
    type: String,
    required: true,
  },
  author: {
    type: Schema.Types.ObjectId,
    ref: 'User',
    required: true,
  },
  channel: {
    type: Schema.Types.ObjectId,
    ref: 'Channel',
    required: true,
  },
});

// TODO: Remove Message from list in parent Channel

module.exports = mongoose.model('Message', MessageSchema);
