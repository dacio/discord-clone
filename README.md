# Harmony

This is a WIP "clone" of [Discord](https://discordapp.com/) using WebSockets.

## Features

Has:
- Guilds
- Channels
- Messages
- Users
- WebSocket Updates

Lacks:
- Permissions
- Message searches
- User previews
- File uploads
- Adherence to Material design guidelines
- Embedded

## Technologies Used
- Backend
  - NodeJS
  - Express
  - Mongoose/MongoDB
- Frontend
  - React
  - Redux
    - Ducks
    - Thunks
  - Material UI
- JSONWebTokens
- WebSockets (Socket.IO)

## Getting Started

```sh
$ npm install
```

### Variables
| Name | Purpose | Default | Notes |
| --- | --- | --- | --- |
| PORT | Port server listens on | 5000 | Only in production |
| MONGODB_URI | MongoDB server Mongoose interacts with | mongodb://localhost/discord-clone | |
| SECRET | Secrets JWTs are hashed with | N/A | |

## Production

````sh
$ npm start
````

Live at: localhost:`PORT`

## Development (live updates)

```sh
$ npm run dev
```

Live at: localhost:3000
